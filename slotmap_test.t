Test slotmap
  $ $SCHMU -m $TESTDIR/slotmap.smu
  $ $SCHMU $TESTDIR/slotmap_test.smu
  $ valgrind -q --leak-check=yes ./slotmap_test
  # slotmap
  ## basic
  20
  30
  none
  erase
  none
  none
  20
  40
  40
  40
  50
  iter-values
  50
  55
  40
  ## gen
  is odd
  is even
  is odd
  ## iter
  key: {:idx 0 :gen 1}
  value: 10
  key: {:idx 1 :gen 1}
  value: 20
  key: {:idx 2 :gen 1}
  value: 30
  after erase
  key: {:idx 0 :gen 1}
  value: 10
  key: {:idx 1 :gen 3}
  value: 30
  key: {:idx 2 :gen 3}
  value: 20
  ## filter-update
  key: {:idx 0 :gen 1}
  value: 10
  key: {:idx 1 :gen 1}
  value: 20
  key: {:idx 2 :gen 1}
  value: 30
  key: {:idx 3 :gen 1}
  value: 40
  filter out 30
  key: {:idx 0 :gen 1}
  value: 11
  key: {:idx 1 :gen 1}
  value: 21
  key: {:idx 3 :gen 1}
  value: 41
  filter out 31 again, nothing should be deleted
  length: 3
  filter out 42
  key: {:idx 0 :gen 1}
  value: 13
  key: {:idx 1 :gen 1}
  value: 23
  length: 2
  ## update-key
  k1: 10
  k2: 20
  k1: 11
  k2: 20
  k1: 11
  k2: 22
